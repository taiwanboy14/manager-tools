const fs = require('fs');
const path = require('path');
const axios = require('axios');

async function downloadTeamYML() {
  const url = 'https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml';
  const outpath = path.resolve(__dirname, 'team.yml');
  const writer = fs.createWriteStream(outpath);

  const response = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  });

  response.data.pipe(writer);

  return new Promise((resolve, reject) => {
    writer.on('finish', resolve);
    writer.on('error', reject);
  });
}

downloadTeamYML();
